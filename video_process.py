# Capture, process and display video real-time. 
# Currently this is doing object tracking by color

# Quit by pressing 'q' while the focus is on the video display window 
# From http://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_gui/py_video_display/py_video_display.html 

import numpy as np
import cv2

cap = cv2.VideoCapture(0)
# yellow highlighter: lower: 30,50,50  upper: 40,255,255
lower = np.array([30,50,50])
upper = np.array([40,255,255])
kernel = np.ones((20,20), np.uint8)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    # gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, lower, upper)
    masked = cv2.bitwise_and(frame, frame, mask=mask)
    ret,thresh = cv2.threshold(mask, 1,255, cv2.THRESH_BINARY) 
    opened = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
    # opened_gray = cv2.cvtColor(opened, cv2.COLOR_RGB2GRAY)
    blur = cv2.blur(opened, (5,5))
    image, contours, hierarchy = cv2.findContours(opened, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    # Display the resulting frame
    cv2.imshow('frame', blur)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()

